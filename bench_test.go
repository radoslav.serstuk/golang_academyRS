package main_test

import (
	"fmt"
	"testing"
)

var arr []int
var set map[int]int

func init() {

	arr = generateArray()
	set = generateMapper(arr)
}

// go test -benchmem -bench=.  je prikaz na spustenie bench
func BenchmarkMap(t *testing.B) {
	fmt.Println(arr[99])

}

func BenchmarkArray(t *testing.B) {
	fmt.Println(set[99])

}


func generateArray() []int {
	locArr := make([]int, 0, 100)
	for i := 0; i < 100; i++ {
		locArr = append(locArr, i)
	}
	return locArr
}

func generateMapper(locArr []int) map[int]int {
	locSet := make(map[int]int, len(locArr))
	for index, value := range locArr {
		locSet[index] = value
	}
	return locSet
}
